﻿using CTCNode.Core.Models;
using CTCNode.Infrastructure.Context;
using CTCNode.Infrastructure.Interfaces;

namespace CTCNode.Infrastructure.Repository
{
    class TaskListRepository : GenericRepository<TaskList>, ITaskListRepository
    {
        #region Constructors
        public TaskListRepository(NodeContext context)
            : base(context)
        {

        }

        #endregion

        #region Properties
        public NodeContext NodeContext
        {
            get
            {
                return Context as NodeContext;

            }
        }
        #endregion
    }
}
