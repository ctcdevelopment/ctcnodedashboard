﻿using CTCNode.Core.Models;
using CTCNode.Infrastructure.Context;
using CTCNode.Infrastructure.Repository;

namespace CTCNode.Infrastructure.Interfaces
{
    public class HeartBeatRepository : GenericRepository<HeartBeat>, IHeartBeatRepository
    {
        #region Constructors
        public HeartBeatRepository(NodeContext context)
            : base(context)
        {

        }
        #endregion

        #region Properties
        public NodeContext NodeContext
        {
            get
            { return Context as NodeContext; }
            #endregion
        }
    }
}

