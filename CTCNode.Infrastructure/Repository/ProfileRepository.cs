﻿using CTCNode.Core.Models;
using CTCNode.Infrastructure.Context;
using CTCNode.Infrastructure.Interfaces;

namespace CTCNode.Infrastructure.Repository
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {
        #region constructors     
        public ProfileRepository(NodeContext context)
            : base(context)
        {

        }
        #endregion

        #region Properties
        public NodeContext NodeContext
        {
            get
            {
                return Context as NodeContext;
            }
        }

        #endregion

    }
}
