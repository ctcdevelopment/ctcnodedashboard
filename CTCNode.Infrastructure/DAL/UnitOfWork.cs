﻿using CTCNode.Infrastructure.Context;
using CTCNode.Infrastructure.Interfaces;
using CTCNode.Infrastructure.Repository;
using System;
using System.Data.Entity.Validation;

namespace CTCNode.Infrastructure.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Constructors
        public UnitOfWork(NodeContext context)
        {
            _context = context;
            AddRepos();
        }
        #endregion

        #region Members
        private bool disposed = false;
        private readonly NodeContext _context;
        private string _errorMessage;
        #endregion

        #region Properties
        public IHeartBeatRepository HeartBeats
        {
            get;
            private set;
        }

        public ITaskListRepository Tasks
        {
            get;
            private set;
        }
        public IProfileRepository Profiles
        {
            get;
            private set;
        }
        public string ErrorMessages
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public bool DBExists()
        {
            return _context.Database.Exists();
        }
        #endregion

        #region Methods
        private void AddRepos()
        {
            HeartBeats = new HeartBeatRepository(_context);
            Profiles = new ProfileRepository(_context);
            Tasks = new TaskListRepository(_context);
        }

        public int Complete()
        {
            try
            {
                return _context.SaveChanges();


            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;
                    }

                }
                throw new Exception(_errorMessage, dbEx);

            }
        }
        #endregion

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }


        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
