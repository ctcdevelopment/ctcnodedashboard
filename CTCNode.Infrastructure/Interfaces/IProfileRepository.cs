﻿using CTCNode.Core.Models;
using CTCNode.Infrastructure.Context;

namespace CTCNode.Infrastructure.Interfaces
{
    public interface IProfileRepository : IGenericRepository<Profile>
    {
        NodeContext NodeContext { get; }
    }
}
