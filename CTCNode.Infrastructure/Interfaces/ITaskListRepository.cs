﻿using CTCNode.Core.Models;
using CTCNode.Infrastructure.Context;

namespace CTCNode.Infrastructure.Interfaces
{
    public interface ITaskListRepository : IGenericRepository<TaskList>
    {
        NodeContext NodeContext { get; }
    }
}
