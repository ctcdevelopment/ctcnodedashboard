﻿using CTCNode.Core.Models;
using CTCNode.Infrastructure.Context;

namespace CTCNode.Infrastructure.Interfaces
{
    public interface IHeartBeatRepository : IGenericRepository<HeartBeat>
    {
        NodeContext NodeContext { get; }
    }
}