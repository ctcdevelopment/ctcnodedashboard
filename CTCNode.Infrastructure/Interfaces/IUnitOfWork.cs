﻿using System;

namespace CTCNode.Infrastructure.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IHeartBeatRepository HeartBeats { get; }
        IProfileRepository Profiles { get; }
        ITaskListRepository Tasks { get; }

        string ErrorMessages { get; set; }

        int Complete();
        bool DBExists();
        void Dispose();
        void Dispose(bool disposing);


    }
}