﻿using CTCNode.Core.Models;
using System.Data.Entity;

namespace CTCNode.Infrastructure.Context
{
    public class NodeContext : DbContext
    {
        #region Constructors


        public NodeContext()
            : base("name=NodeData")
        {

        }
        #endregion

        #region Methods

        #endregion

        #region Properties
        public virtual DbSet<HeartBeat> HeartBeats { get; set; }
        public virtual DbSet<TaskList> TaskLists { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }

        #endregion
    }
}
