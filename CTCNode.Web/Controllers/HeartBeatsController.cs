﻿using CTCNode.Core.Models;
using CTCNode.Infrastructure.Context;
using CTCNode.Infrastructure.DAL;
using CTCNode.Infrastructure.Interfaces;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace CTCNode.Web.Controllers
{
    public class HeartBeatsController : Controller
    {
        // private NodeContext db = new NodeContext();
        private IUnitOfWork db = new UnitOfWork(new NodeContext());

        // GET: HeartBeats
        public ActionResult Index()
        {
            return View(db.HeartBeats.GetAll());
        }

        // GET: HeartBeats/Details/5
        public ActionResult Details(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HeartBeat heartBeat = db.HeartBeats.Find(x => x.HB_ID == id).FirstOrDefault();
            if (heartBeat == null)
            {
                return HttpNotFound();
            }
            return View(heartBeat);
        }

        // GET: HeartBeats/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HeartBeats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HB_ID,HB_C,HB_NAME,HB_PATH,HB_ISMONITORED,HB_PID,HB_LASTOPERATION,HB_LASTCHECKIN,HB_OPENED,HB_LASTOPERATIONPARAMS")] HeartBeat heartBeat)
        {
            if (ModelState.IsValid)
            {
                heartBeat.HB_ID = Guid.NewGuid();
                db.HeartBeats.Add(heartBeat);
                db.Complete();
                return RedirectToAction("Index");
            }

            return View(heartBeat);
        }

        // GET: HeartBeats/Edit/5



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
